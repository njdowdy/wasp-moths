import os
import io
import base64
import numpy as np
import logging
import plotly.express as px
import plotly.graph_objects as go
from sklearn.manifold import TSNE
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
from PIL import Image

# Setup logging
logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)

# Define paths
image_dir = "src/images/main-processed-cropped"  # Change this to your image directory
output_features_file = "features-main.npy"
perplexity = 25
iterations = 5000
output_plot_file = f"tsne_plot-main-n{iterations}-p{perplexity}.html"


# Load and preprocess images
def load_and_preprocess_image(img_path, target_size=(224, 224)):
    logging.info(f"Loading and preprocessing image: {img_path}")
    img = image.load_img(img_path, target_size=target_size)
    img_array = image.img_to_array(img)
    img_array = np.expand_dims(img_array, axis=0)
    img_array = preprocess_input(img_array)
    return img_array


# Extract features using VGG16
def extract_features(model, img_array):
    features = model.predict(img_array)
    return features.flatten()


# Recursively get all image paths
def get_image_paths(root_dir):
    image_paths = []
    for root, _, files in os.walk(root_dir):
        for file in files:
            if file.lower().endswith((".png", ".jpg", ".jpeg")):
                image_paths.append(os.path.join(root, file))
    return image_paths


# Initialize VGG16 model
logging.info("Initializing VGG16 model")
base_model = VGG16(weights="imagenet")
model = Model(inputs=base_model.input, outputs=base_model.get_layer("fc2").output)

# Load all images and extract features if not already saved
image_paths = get_image_paths(image_dir)
if os.path.exists(output_features_file):
    logging.info(f"Loading features from file: {output_features_file}")
    features = np.load(output_features_file)
else:
    logging.info("Loading image paths")
    features = []

    for img_path in image_paths:
        img_array = load_and_preprocess_image(img_path)
        img_features = extract_features(model, img_array)
        features.append(img_features)
        logging.info(f"Extracted features for image: {img_path}")

    features = np.array(features)
    # Save features to a file
    logging.info(f"Saving features to file: {output_features_file}")
    np.save(output_features_file, features)

# Apply t-SNE to reduce dimensionality to 2D
logging.info("Applying t-SNE")

tsne = TSNE(n_components=2, random_state=42, perplexity=perplexity, n_iter=iterations)
features_2d = tsne.fit_transform(features)

# Create a DataFrame with the results and the image paths
import pandas as pd

# Create a DataFrame with the results and the image paths
df = pd.DataFrame(features_2d, columns=["x", "y"])
df["image_path"] = image_paths


# Function to convert images to base64 after downsampling and compression
def encode_image(image_path):
    with Image.open(image_path) as img:
        # Downsample to 20% of original size
        img = img.resize((img.width // 5, img.height // 5), Image.LANCZOS)
        # Save to a buffer
        buffer = io.BytesIO()
        img.save(
            buffer, format="PNG", optimize=True
        )  # Save as PNG to retain transparency
        # Encode to base64
        encoded = base64.b64encode(buffer.getvalue()).decode()
    return f"data:image/png;base64,{encoded}"


# Encode images
df["encoded_image"] = df["image_path"].apply(encode_image)

# Create an interactive plot with plotly
logging.info("Creating interactive plot")
fig = px.scatter(df, x="x", y="y", hover_data=["image_path"])

# Add images to the plot
for i, row in df.iterrows():
    fig.add_layout_image(
        dict(
            source=row["encoded_image"],
            x=row["x"],
            y=row["y"],
            xref="x",
            yref="y",
            sizex=1,  # Adjust the size as needed
            sizey=1,  # Adjust the size as needed
            xanchor="center",
            yanchor="middle",
            layer="above",
        )
    )

# Update the layout for better visualization
fig.update_layout(
    xaxis=dict(showgrid=False),
    yaxis=dict(showgrid=False),
)

# Save the plot to an HTML file
logging.info(f"Saving plot to file: {output_plot_file}")
fig.write_html(output_plot_file)

logging.info("Script completed successfully")
