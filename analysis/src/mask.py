import os
import cv2
import numpy as np


def binarize_mask(mask: np.ndarray, threshold: int = 128) -> np.ndarray:
    _, binary_mask = cv2.threshold(mask, threshold, 255, cv2.THRESH_BINARY)
    return binary_mask


def apply_mask(image_path: str, mask_path: str, output_path: str) -> None:
    # Open the input image and mask
    image = cv2.imread(image_path, cv2.IMREAD_UNCHANGED)
    mask = cv2.imread(mask_path, cv2.IMREAD_GRAYSCALE)

    # Invert the mask since we made the moth black and the background white
    mask = cv2.bitwise_not(mask)

    # Ensure mask is same size as image
    mask = cv2.resize(mask, (image.shape[1], image.shape[0]))

    # Binarize the mask to make sure it is perfectly binary
    mask = binarize_mask(mask)

    # If the image does not have an alpha channel, add one
    if image.shape[2] == 3:
        image = cv2.cvtColor(image, cv2.COLOR_BGR2BGRA)

    # Replace the alpha channel with the mask
    image[:, :, 3] = mask

    # Save the output image
    cv2.imwrite(output_path, image)


def process_images(input_folder: str, output_folder: str) -> None:
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    for file_name in os.listdir(input_folder):
        if file_name.endswith((".jpg", ".jpeg", ".png")) and not file_name.endswith(
            "_sil.jpg"
        ):
            base_name = os.path.splitext(file_name)[0]
            output_file_path = os.path.join(output_folder, f"{base_name}.png")

            # Skip processing if the output file already exists
            if os.path.exists(output_file_path):
                print(f"Skipping {file_name}, output already exists.")
                continue

            image_path = os.path.join(input_folder, file_name)
            mask_path = os.path.join(input_folder, f"{base_name}_sil.jpg")
            if os.path.exists(mask_path):
                apply_mask(image_path, mask_path, output_file_path)
                print(f"Processed {file_name} with mask {base_name}_sil.jpg")
            else:
                print(f"Mask for {file_name} not found.")


if __name__ == "__main__":
    input_folder = "src/images/main"
    output_folder = "src/images/main-processed"
    process_images(input_folder, output_folder)
