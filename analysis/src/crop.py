import os
from PIL import Image


def crop_image(
    input_path: str, output_path: str, square_aspect_ratio: bool = False
) -> None:
    """
    Crop the image to the maximum extent of non-transparent pixels.

    Parameters:
    - input_path (str): Path to the input image.
    - output_path (str): Path to save the cropped image.
    - square_aspect_ratio (bool): Whether to expand the crop to a square aspect ratio. Default is False.
    """
    img = Image.open(input_path).convert("RGBA")
    bbox = img.getbbox()  # Get bounding box of non-transparent pixels
    if bbox:
        if square_aspect_ratio:
            # Calculate the square bounding box
            left, upper, right, lower = bbox
            width = right - left
            height = lower - upper
            if width > height:
                diff = width - height
                upper -= diff // 2
                lower += diff // 2
                # Adjust for odd differences to keep it centered
                if diff % 2 != 0:
                    lower += 1
            else:
                diff = height - width
                left -= diff // 2
                right += diff // 2
                # Adjust for odd differences to keep it centered
                if diff % 2 != 0:
                    right += 1
            # Ensure the new bounding box is within image bounds
            left = max(0, left)
            upper = max(0, upper)
            right = min(img.width, right)
            lower = min(img.height, lower)
            bbox = (left, upper, right, lower)

        cropped_img = img.crop(bbox)
        cropped_img.save(output_path)
        print(f"Cropped image saved to {output_path}")
    else:
        print(f"No non-transparent pixels found in {input_path}")


def process_images(
    input_folder: str, output_folder: str, square_aspect_ratio: bool = False
) -> None:
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    for file_name in os.listdir(input_folder):
        if file_name.endswith((".jpg", ".jpeg", ".png")) and not file_name.endswith(
            "_crop.jpg"
        ):
            base_name = os.path.splitext(file_name)[0]
            image_path = os.path.join(input_folder, file_name)
            output_path = os.path.join(output_folder, f"{base_name}_crop.png")

            # Skip processing if the output file already exists
            if os.path.exists(output_path):
                print(f"Skipping {file_name}, output already exists.")
                continue

            crop_image(image_path, output_path, square_aspect_ratio)


if __name__ == "__main__":
    input_folder = "src/images/main-processed"
    output_folder = "src/images/main-processed-cropped"
    square_aspect_ratio = True  # Set this to True if you want square aspect ratio crops
    process_images(input_folder, output_folder, square_aspect_ratio)
